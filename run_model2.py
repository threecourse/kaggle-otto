from runner import Runner
from model_xgb import ModelSetXgb
import model_keras

keras_params_h2 = {}
keras_params_h2['lr'] = 0.01
keras_params_h2['momentum'] = 0.9
keras_params_h2['h1'] = 500
keras_params_h2['h2'] = 100
keras_params_h2['dropout1'] = 0.2
keras_params_h2['dropout2'] = 0.2
keras_params_h2['nb_epochs'] = 50
keras_params_h2['batch_size'] = 256
keras_params_h2['hidden_layers'] = 2

keras_params_h3 = {}
keras_params_h3['lr'] = 0.01
keras_params_h3['momentum'] = 0.9
keras_params_h3['h1'] = 500
keras_params_h3['h2'] = 300
keras_params_h3['h3'] = 100
keras_params_h3['dropout1'] = 0.5
keras_params_h3['dropout2'] = 0.1
keras_params_h3['dropout3'] = 0.1
keras_params_h3['nb_epochs'] = 30
keras_params_h3['batch_size'] = 256
keras_params_h3['hidden_layers'] = 3

keras_params_nnb = dict(keras_params_h3)
keras_params_nnc = dict(keras_params_h3)
keras_params_nnd = dict(keras_params_h3)
keras_params_nne = dict(keras_params_h3)
keras_params_nne['nb_epochs'] = 50

keras_params_small = dict(keras_params_h2)
keras_params_small['nb_epochs'] = 5

keras_params_nnb_he = dict(keras_params_h3)
keras_params_nnb_he["init"] = "he_normal"

keras_params_nnb2 = dict(keras_params_h3)

"""
Runner.run(model_keras.ModelSetKeras_basic, "keras_basic", None, keras_params_h2, train=True, test=True)
Runner.run(model_keras.ModelSetKeras_basic, "keras_basic_h3", None, keras_params_h3, train=True, test=True)
"""

# Runner.run(model_keras.ModelSetKeras_basic, "keras_small", None, keras_params_small, train=True, test=True)
# Runner.run(model_keras.ModelSetKeras_ens, "keras_ens", None, keras_params_small, train=True, test=True)

# Runner.run(model_keras.ModelSetKeras_nnb, "keras_nnb", None, keras_params_nnb, train=True, test=True)
# Runner.run(model_keras.ModelSetKeras_nnb, "keras_nnb", None, keras_params_nnb, train=False, test=True)

Runner.run(model_keras.ModelSetKeras_nnb, "keras_nnb_he", None, keras_params_nnb_he, train=True, test=True)

"""
Runner.run(model_keras.ModelSetKeras_nnc, "keras_nnc", None, keras_params_nnc, train=True, test=True)
Runner.run(model_keras.ModelSetKeras_nnd, "keras_nnd", None, keras_params_nnd, train=True, test=True)
Runner.run(model_keras.ModelSetKeras_nne, "keras_nne", None, keras_params_nne, train=True, test=True)
"""

