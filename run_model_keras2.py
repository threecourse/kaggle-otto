from runner import Runner
from model_xgb import ModelSetXgb
import model_keras

keras_params_h3 = {}
keras_params_h3['lr'] = 0.01
keras_params_h3['momentum'] = 0.9
keras_params_h3['h1'] = 500
keras_params_h3['h2'] = 300
keras_params_h3['h3'] = 100
keras_params_h3['dropout1'] = 0.5
keras_params_h3['dropout2'] = 0.1
keras_params_h3['dropout3'] = 0.1
keras_params_h3['nb_epochs'] = 70
keras_params_h3['batch_size'] = 256
keras_params_h3['hidden_layers'] = 3

keras_params_h3_bags = dict(keras_params_h3)
keras_params_h3_bags['bags'] = 30

keras_params_small = dict(keras_params_h3)
keras_params_small['nb_epochs'] = 5
keras_params_small['bags'] = 3

Runner.run(model_keras.ModelSetBaggingKeras_nnb, "keras_nnb_bags", None, keras_params_h3_bags, train=True, test=True)
Runner.run(model_keras.ModelSetBaggingKeras_nnc, "keras_nnc_bags", None, keras_params_h3_bags, train=True, test=True)
Runner.run(model_keras.ModelSetBaggingKeras_nnd, "keras_nnd_bags", None, keras_params_h3_bags, train=True, test=True)
Runner.run(model_keras.ModelSetBaggingKeras_nne, "keras_nne_bags", None, keras_params_h3_bags, train=True, test=True)