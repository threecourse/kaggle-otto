import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import StratifiedKFold
from util import Util
from sklearn.preprocessing import StandardScaler
from data_loading import DataLoading

class DataInitializer:

    @classmethod
    def data_initialize(cls):
        train_file = "../input/train.csv"
        test_file = "../input/test.csv"

        df_train = pd.read_csv(train_file)
        df_train["is_train"] = True
        df_test = pd.read_csv(test_file)
        df_test["target"] = "Class_1" # dummy
        df_test["is_train"] = False
        df = pd.concat([df_train, df_test]).reset_index(drop=True)
        encoder = LabelEncoder()
        df["target"] = encoder.fit_transform(df["target"]).astype(np.int32)

        id, feature, target, is_train = df.iloc[:, 0], df.iloc[:, 1:-2], df.iloc[:, -2], df.iloc[:, -1]
        Util.dumpc(id, "../model/idx/id.pkl")
        Util.dumpc(feature, "../model/idx/feature.pkl")
        print type(feature), feature.shape

        Util.dumpc(target, "../model/idx/target.pkl")
        Util.dumpc(is_train, "../model/idx/is_train.pkl")

    @classmethod
    def write_cv_index(cls):
        target = DataLoading.load_target_all()
        is_train = DataLoading.load_is_train_all()
        n_folds = 5
        folds = list(StratifiedKFold(target[is_train], n_folds=n_folds, shuffle=True, random_state=71))

        # train, validation
        np.random.seed(71)
        for i_fold, (tr, te) in enumerate(folds):
            idx_tr = np.random.permutation(tr)
            idx_te = te
            Util.dumpc(idx_tr, "../model/idx/train_{}.pkl".format(i_fold))
            Util.dumpc(idx_te, "../model/idx/test_{}.pkl".format(i_fold))
            print i_fold, "train", idx_tr.shape, idx_tr[:5], idx_tr[-5:]
            print i_fold, "test", idx_te.shape, idx_te[:5], idx_te[-5:]

        # train, test
        np.random.seed(71)
        for i_fold in [-1]:
            n_train = is_train.sum()
            n_all = len(is_train)
            print "n_train:{}, n_all:{}".format(n_train, n_all)
            tr = np.array(range(n_train))
            te = np.array(range(n_train, n_all))
            idx_tr = np.random.permutation(tr)
            idx_te = te
            Util.dumpc(idx_tr, "../model/idx/train_{}.pkl".format(i_fold))
            Util.dumpc(idx_te, "../model/idx/test_{}.pkl".format(i_fold))
            print i_fold, "train", idx_tr.shape, idx_tr[:5], idx_tr[-5:]
            print i_fold, "test", idx_te.shape, idx_te[:5], idx_te[-5:]

if __name__ == "__main__":
    DataInitializer.data_initialize()
    DataInitializer.write_cv_index()