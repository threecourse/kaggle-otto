from runner import Runner
from model_xgb import ModelSetXgb
import model_keras

keras_params_h2 = {}
keras_params_h2['lr'] = 0.01
keras_params_h2['momentum'] = 0.9
keras_params_h2['h1'] = 100
keras_params_h2['h2'] = 50
keras_params_h2['dropout1'] = 0.2
keras_params_h2['dropout2'] = 0.2
keras_params_h2['nb_epochs'] = 100
keras_params_h2['batch_size'] = 256
keras_params_h2['hidden_layers'] = 2

keras_params_h2_2 = dict(keras_params_h2)
keras_params_h2_2['lr'] = 0.001

keras_params_h2_3 = dict(keras_params_h2)
keras_params_h2_3['lr'] = 0.1


keras_params_h3 = {}
keras_params_h3['lr'] = 0.01
keras_params_h3['momentum'] = 0.9
keras_params_h3['h1'] = 500
keras_params_h3['h2'] = 300
keras_params_h3['h3'] = 100
keras_params_h3['dropout1'] = 0.5
keras_params_h3['dropout2'] = 0.1
keras_params_h3['dropout3'] = 0.1
keras_params_h3['nb_epochs'] = 90
keras_params_h3['batch_size'] = 256
keras_params_h3['hidden_layers'] = 3

# Runner.run(model_keras.ModelSetKeras_basic, "keras0", None, keras_params_h2, train=True, test=True)
# Runner.run(model_keras.ModelSetKeras_basic, "keras0_2", None, keras_params_h2_2, train=True, test=True)
# Runner.run(model_keras.ModelSetKeras_basic, "keras0_3", None, keras_params_h2_3, train=True, test=True)

Runner.run(model_keras.ModelSetKeras_basic, "kerasD", None, keras_params_h3, train=True, test=True)
