import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import sys
import random
import os
from sklearn.metrics import log_loss
from util import Util
from sklearn.preprocessing import StandardScaler

import model
from model import Model, ModelSet, ModelSetBagging
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import Adadelta, SGD
from keras.layers.normalization import BatchNormalization
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint
from keras.utils.np_utils import to_categorical
from feature import Feature
from feature import Transform
from data_loading import DataLoading

class Config:
    n_folds = 5


class ModelKeras(Model):
    def __init__(self):
        self.nb_classes = 9

    def set_params(self, prms):
        self.prms = prms

        self.nb_epochs = self.prms["nb_epochs"]
        if self.nb_epochs is None:
            self.nb_epochs = 200
            self.earlystopping = EarlyStopping(monitor='val_loss', patience=5, verbose=2)
            self.callbacks = [self.earlystopping]
        else:
            self.callbacks = []

    def set_data(self, x_tr, y_tr, x_te, y_te, is_va):
        self.x_tr = np.array(x_tr)
        self.y_tr = to_categorical(y_tr, self.nb_classes)
        self.is_va = is_va

        if self.is_va:
            self.x_te = np.array(x_te)
            self.y_te = to_categorical(y_te, self.nb_classes)

    def train(self):
        self.construct(self.x_tr.shape[1])
        if self.is_va:
            self.model.fit(self.x_tr, self.y_tr, nb_epoch=self.nb_epochs,
                           batch_size=self.prms["batch_size"], verbose=2, shuffle=True,
                           validation_data=(self.x_te, self.y_te), callbacks=self.callbacks)
        else:
            self.model.fit(self.x_tr, self.y_tr, nb_epoch=self.nb_epochs,
                           batch_size=self.prms["batch_size"], verbose=2, shuffle=True)

    def predict(self, data):
        data = np.array(data)
        return self.model.predict_proba(data, batch_size=128, verbose=2)

    def construct(self, input_dim):

        seed = self.prms["seed"] if self.prms.has_key("seed") else 71
        i_bag = self.prms["i_bag"] if self.prms.has_key("i_bag") else 0
        np.random.seed(seed + i_bag)

        if not self.prms.has_key("init"):
            init = 'glorot_normal'
        else:
            init = self.prms["init"]

        activation = 'relu'
        num_classes = 9

        optimizer = SGD(lr=self.prms["lr"], momentum=self.prms["momentum"], nesterov=True)

        hidden_layers = self.prms["hidden_layers"]
        assert (hidden_layers in [2, 3])

        model = Sequential()
        model.add(Dense(self.prms["h1"], input_dim=input_dim, init=init))
        model.add(Activation(activation))
        model.add(Dropout(self.prms["dropout1"]))
        model.add(Dense(self.prms["h2"], init=init))
        model.add(Activation(activation))
        model.add(Dropout(self.prms["dropout2"]))
        if hidden_layers == 3:
            model.add(Dense(self.prms["h3"], init=init))
            model.add(Activation(activation))
            model.add(Dropout(self.prms["dropout3"]))
        model.add(Dense(self.nb_classes, init=init))
        model.add(Activation('softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=optimizer)

        self.model = model


class ModelSetKeras_basic(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_feature(i_fold, scaling=True)

    def get_model(self):
        return ModelKeras()


class ModelSetKeras_nnb(ModelSet):
    """nnb model"""

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnb, scaling=True)

    def get_model(self):
        return ModelKeras()


class ModelSetKeras_nnc(ModelSet):
    """nnb model"""

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnc, scaling=True)

    def get_model(self):
        return ModelKeras()


class ModelSetKeras_nnd(ModelSet):
    """nnb model"""

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnd, scaling=True)

    def get_model(self):
        return ModelKeras()

class ModelSetKeras_nne(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nne, scaling=True)

    def get_model(self):
        return ModelKeras()


class ModelSetKeras_dummy(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.dummy, scaling=True)

    def get_model(self):
        return ModelKeras()


class ModelSetKeras_ens(ModelSet):

    def load_x(self, i_fold):
        idx_tr, idx_te = DataLoading.load_index(i_fold)
        values = Feature.load_stacking_values("keras_small")
        values = StandardScaler().fit_transform(values)
        return values[idx_tr, :], values[idx_te, :]

    def get_model(self):
        return ModelKeras()

class ModelSetBaggingKeras_nnb(ModelSetBagging):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnb, scaling=True)

    def get_model(self):
        return ModelKeras()

class ModelSetBaggingKeras_nnc(ModelSetBagging):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnc, scaling=True)

    def get_model(self):
        return ModelKeras()

class ModelSetBaggingKeras_nnd(ModelSetBagging):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nnd, scaling=True)

    def get_model(self):
        return ModelKeras()

class ModelSetBaggingKeras_nne(ModelSetBagging):

    def load_x(self, i_fold):
        return Feature.load_feature_transform(i_fold, Transform.nne, scaling=True)

    def get_model(self):
        return ModelKeras()