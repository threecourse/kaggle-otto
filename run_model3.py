from runner import Runner
from model_xgb import ModelSetXgb
import model_keras

keras_params_h2 = {}
keras_params_h2['lr'] = 0.01
keras_params_h2['momentum'] = 0.9
keras_params_h2['h1'] = 500
keras_params_h2['h2'] = 100
keras_params_h2['dropout1'] = 0.2
keras_params_h2['dropout2'] = 0.2
keras_params_h2['nb_epochs'] = 50
keras_params_h2['batch_size'] = 256
keras_params_h2['hidden_layers'] = 2

keras_params_h3_2 = {}
keras_params_h3_2['lr'] = 0.01
keras_params_h3_2['momentum'] = 0.9
keras_params_h3_2['h1'] = 500
keras_params_h3_2['h2'] = 300
keras_params_h3_2['h3'] = 100
keras_params_h3_2['dropout1'] = 0.5
keras_params_h3_2['dropout2'] = 0.1
keras_params_h3_2['dropout3'] = 0.1
keras_params_h3_2['nb_epochs'] = 30
keras_params_h3_2['batch_size'] = 256
keras_params_h3_2['hidden_layers'] = 3

keras_params_nne = dict(keras_params_h3_2)
keras_params_nne['nb_epochs'] = 70
keras_params_nne['bags'] = 10

keras_params_small = dict(keras_params_h2)
keras_params_small['nb_epochs'] = 5
keras_params_small['bags'] = 3

Runner.run(model_keras.ModelSetBaggingKeras_nne, "keras_nne_bags", None, keras_params_nne, train=True, test=True)

