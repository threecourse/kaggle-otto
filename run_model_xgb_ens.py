from runner import Runner
import model_xgb

xgb_params = {}
xgb_params['objective'] = 'multi:softprob'
xgb_params['eta'] = 0.125
xgb_params['max_depth'] = 4
xgb_params['eval_metric'] = 'mlogloss'
xgb_params['silent'] = 1
# xgb_params['nthread'] = 6
xgb_params['num_class'] = 9
xgb_params['subsample'] = 0.7
xgb_params['colsample_bylevel'] = 0.12
xgb_params['colsample_bytree'] = 1.0
xgb_params['min_child_weight'] = 1
xgb_params['seed'] = 71
xgb_params["n_rounds"] = 110 # can go more

xgb_params_small = dict(xgb_params)
xgb_params_small["n_rounds"] = 5

# transform
# min child weight: 1, 5, 10, 15, 20, 25, 30, 35, 40;

flist = ["keras_nnb_bags", "keras_nnc_bags", "keras_nnd_bags", "keras_nne_bags"]
flist += ["xgb_xgbz{}".format(i) for i in range(10)]
Runner.run(model_xgb.ModelSetXgb_ens, "xgb_ens2", flist, xgb_params, train=True, test=True)
