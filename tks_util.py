import numpy as np
import scipy as sp
import pandas as pd

# mat: A sparse matrix
def remove_duplicate_cols(mat):
    if not isinstance(mat, sp.sparse.coo_matrix):
        mat = mat.tocoo()
    row = mat.row
    col = mat.col
    data = mat.data
    crd = pd.DataFrame({'row': row, 'col': col, 'data': data}, columns=['col', 'row', 'data'])
    col_rd = crd.groupby('col').apply(lambda x: str(np.array(x)[:, 1:]))
    dup = col_rd.duplicated()
    return mat.tocsc()[:, col_rd.index.values[dup.values == False]]

# Return a sparse matrix whose column has k_min to k_max 1s
def col_k_ones_matrix(p, m, k=None, k_min=1, k_max=1, seed=None, rm_dup_cols=True):
    if k is not None:
        k_min = k_max = k
    if seed is not None: np.random.seed(seed)
    k_col = np.random.choice(range(k_min, k_max + 1), m)
    col = np.repeat(range(m), k_col)
    popu = np.arange(p)
    l = [np.random.choice(popu, k_col[i], replace=False).tolist() for i in range(m)]
    row = sum(l, [])
    data = np.ones(k_col.sum())
    mat = sp.sparse.coo_matrix((data, (row, col)), shape=(p, m), dtype=np.float32)
    if rm_dup_cols:
        mat = remove_duplicate_cols(mat)
    return mat
