import numpy as np
import pandas as pd
from sklearn.externals import joblib
import os
import datetime
import sklearn.metrics

class Config:
    n_folds = 5
    num_classes = 9

class Util:
    num_classes = 9

    @classmethod
    def mkdir(cls, dr):
        if not os.path.exists(dr):
            os.makedirs(dr)

    @classmethod
    def mkdir_file(cls, path):
        dr = os.path.dirname(path)
        if not os.path.exists(dr):
            os.makedirs(dr)

    @classmethod
    def dump(cls, obj, filename, compress=0):
        cls.mkdir_file(filename)
        joblib.dump(obj, filename, compress=compress)

    @classmethod
    def dumpc(cls, obj, filename):
        cls.mkdir_file(filename)
        cls.dump(obj, filename, compress=3)

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def to_csv(cls, _df, filename, index=False, sep="\t"):
        cls.mkdir_file(filename)
        _df.to_csv(filename, sep=sep, index=index)

    @classmethod
    def nowstr(cls):
        return str(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))

    @classmethod
    def nowstrhms(cls):
        return str(datetime.datetime.now().strftime("%H-%M-%S"))

    @classmethod
    def to_ltsv(cls, ordered_dict):
        return "\t".join(["{}:{}".format(key, value) for key, value in ordered_dict.items()])


class Evaluation:

    @classmethod
    def log_loss(cls, actual, pred):
        return sklearn.metrics.log_loss(actual, pred)

    @classmethod
    def accuracy(cls, actual, pred):
        pred_label = np.argmax(pred, axis=1)
        return sklearn.metrics.accuracy_score(actual, pred_label)