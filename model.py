import numpy as np
import pandas as pd
from data_loading import DataLoading

class Model(object):
    """ manages model"""

    def __init__(self):
        pass

    def set_params(self, prms):
        pass

    def set_data(self, x_tr, y_tr, x_te, y_te, is_va):
        pass

    def train(self):
        pass

    def predict(self, data):
        pass


class ModelSet(object):
    """ manages model, parameters and features"""

    def __init__(self, name, flist, params, i_fold):
        self.name = name
        self.flist = flist
        self.params = params
        self.i_fold = i_fold
        self.is_va = (self.i_fold != -1)
        self.x_tr, self.x_te = self.load_x(self.i_fold)
        self.y_tr, self.y_te = self.load_y(self.i_fold)

    def train(self):
        self.model = self.get_model()
        self.model.set_params(self.params)
        self.model.set_data(self.x_tr, self.y_tr, self.x_te, self.y_te, is_va=self.is_va)
        self.model.train()

    def predict_te(self):
        return self.model.predict(self.x_te)

    def get_model(self):
        raise NotImplementedError

    def load_x(self, i_fold):
        raise NotImplementedError

    def load_y(self, i_fold):
        return DataLoading.load_target(i_fold)


class ModelSetBagging(object):
    """ manages model, parameters and features"""

    def __init__(self, name, flist, params, i_fold):
        self.name = name
        self.flist = flist
        self.params = params
        self.i_fold = i_fold
        self.is_va = (self.i_fold != -1)
        self.x_tr, self.x_te = self.load_x(self.i_fold)
        self.y_tr, self.y_te = self.load_y(self.i_fold)
        self.bags = self.params["bags"]

    def train(self):
        self.models = []
        for i in range(self.bags):
            print "train bag {}".format(i)
            model = self.get_model()
            self.params["i_bag"] = i
            model.set_params(self.params)
            model.set_data(self.x_tr, self.y_tr, self.x_te, self.y_te, is_va=self.is_va)
            model.train()
            self.models.append(model)

    def predict_te(self):
        predicts = []
        for i in range(self.bags):
            model = self.models[i]
            predicts.append(model.predict(self.x_te))

        pred = np.zeros(predicts[0].shape)
        for i in range(self.bags):
            pred += predicts[i] / float(self.bags)
        return pred

    def get_model(self):
        raise NotImplementedError

    def load_x(self, i_fold):
        raise NotImplementedError

    def load_y(self, i_fold):
        return DataLoading.load_target(i_fold)