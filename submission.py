import pandas as pd
import numpy as np
from util import Util
from data_loading import DataLoading

class Submission:

    @classmethod
    def make_submission(cls, name, pred):

        _, id_test = DataLoading.load_id(-1)
        nowstr = Util.nowstr()
        index = pd.Series(id_test, name="id")
        df = pd.DataFrame(pred, index=index, columns=["Class_{}".format(i) for i in range(1, 10)])
        Util.mkdir("../submission")
        df.to_csv("../submission/subm_{}_{}.csv".format(name, nowstr), index=True, sep=",")

