import logging

class Log:

   @classmethod
   def normal_logger(cls):
       logger = logging.getLogger("normal_logger")
       logger.setLevel(logging.INFO)
       fh = logging.FileHandler('../model/run.log')
       ch = logging.StreamHandler()
       formatter = logging.Formatter('%(asctime)s : %(message)s')
       fh.setFormatter(formatter)
       ch.setFormatter(formatter)
       logger.addHandler(fh)
       logger.addHandler(ch)
       return logger

   @classmethod
   def result_logger(cls):
       logger = logging.getLogger("result_logger")
       logger.setLevel(logging.INFO)
       fh = logging.FileHandler('../model/result.log')
       ch = logging.StreamHandler()
       formatter = logging.Formatter('%(message)s')
       fh.setFormatter(formatter)
       ch.setFormatter(formatter)
       logger.addHandler(fh)
       logger.addHandler(ch)
       return logger