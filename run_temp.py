from runner import Runner
from model_xgb import ModelSetXgb
from model_keras2 import ModelSetKeras2

keras_params = {}
keras_params['lr'] = 0.01
keras_params['momentum'] = 0.9
keras_params['h1'] = 500
keras_params['h2'] = 300
keras_params['h3'] = 100
keras_params['dropout1'] = 0.5
keras_params['dropout2'] = 0.1
keras_params['dropout3'] = 0.1
keras_params['nb_epochs'] = 5 # 70
keras_params['batch_size'] = 256

Runner.run(ModelSetKeras2, "keras2", None, keras_params, test=True)
