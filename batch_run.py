import subprocess

def run(cmd):
    subprocess.call(cmd, shell=True)

run("python run_model_xgb2.py")
run("python run_model_keras2.py")