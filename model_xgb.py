import pandas as pd
import numpy as np
from util import Util
from model import Model, ModelSet
from feature import Feature, Transform
import xgboost as xgb
from data_loading import DataLoading
from sklearn.preprocessing import StandardScaler

class ModelXgb(Model):

    def __init__(self):
        pass

    def set_params(self, prms):
        self.prms = prms

    def set_data(self, x_tr, y_tr, x_te, y_te, is_va):
        self.x_tr = x_tr
        self.y_tr = y_tr
        self.x_te = x_te
        self.y_te = y_te
        self.is_va = is_va

    def train(self):
        dtrain = xgb.DMatrix(self.x_tr, label=self.y_tr, missing=-999.0)
        if self.prms.has_key("n_rounds"):
            n_rounds = self.prms["n_rounds"]
            early_stopping_rounds = None
        else:
            n_rounds = 200
            early_stopping_rounds = 5

        if self.is_va:
            dvalid = xgb.DMatrix(self.x_te, label=self.y_te, missing=-999.0)
            watchlist = [(dtrain, 'train'), (dvalid, 'eval')]
            self.model = xgb.train(self.prms, dtrain, num_boost_round=n_rounds, evals=watchlist,
                                   early_stopping_rounds=early_stopping_rounds)
        else:
            self.model = xgb.train(self.prms, dtrain, num_boost_round=n_rounds)

    def predict(self, data):
        dtest = xgb.DMatrix(data, missing=-999.0)
        return self.model.predict(dtest)


class ModelSetXgb(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_feature(i_fold, scaling=False)

    def get_model(self):
        return ModelXgb()

class ModelSetXgb_xgbz(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_values(i_fold, Transform.xgbz, scaling=False)

    def get_model(self):
        return ModelXgb()


class ModelSetXgb_ens(ModelSet):

    def load_x(self, i_fold):
        idx_tr, idx_te = DataLoading.load_index(i_fold)
        values_list = [Feature.load_stacking_values(name) for name in self.flist]
        values = np.hstack(values_list)
        print values.shape
        return values[idx_tr, :], values[idx_te, :]

    def get_model(self):
        return ModelXgb()
