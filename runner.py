from util import Util, Config, Evaluation
from data_loading import DataLoading
import numpy as np
from collections import OrderedDict
from log import Log
from submission import Submission

nlogger = Log.normal_logger()
rlogger = Log.result_logger()

class Runner:

    @classmethod
    def run(cls, modelclass, name, flist, params, train, test):

        if train:
            lst_logloss = []
            lst_accuracy = []

            for i_fold in range(Config.n_folds):
                model_set = modelclass(name=name, flist=flist, params=params, i_fold=i_fold)
                model_set.train()
                preds = model_set.predict_te()
                _, actual = DataLoading.load_target(i_fold)
                # print preds.shape, actual.shape

                log_loss = Evaluation.log_loss(actual, preds)
                accuracy = Evaluation.accuracy(actual, preds)
                lst_logloss.append(log_loss)
                lst_accuracy.append(accuracy)

                Util.dumpc(preds, "../model/stack/{}_fold{}.pkl".format(name, i_fold))

                nlogger.info("{} - fold:{}, logloss:{}, accuracy:{}".format(name, i_fold, log_loss, accuracy))

            avg_logloss = np.array(lst_logloss).mean()
            avg_accuracy = np.array(lst_accuracy).mean()

            d = OrderedDict( [('name',name), ('logloss',avg_logloss), ('accuracy', avg_accuracy)])
            for i_fold in range(Config.n_folds):
                d["logloss{}".format(i_fold)] = lst_logloss[i_fold]
            for i_fold in range(Config.n_folds):
                d["accuracy{}".format(i_fold)] = lst_accuracy[i_fold]
            rlogger.info(Util.to_ltsv(d))

        if test:
            for i_fold in [-1]:

                model_set = modelclass(name=name, flist=flist, params=params, i_fold=i_fold)
                nlogger.info("{} - start prediction".format(name))
                model_set.train()
                preds = model_set.predict_te()

                Util.dumpc(preds, "../model/stack/{}_fold{}.pkl".format(name, i_fold))
                Submission.make_submission(name, preds)

                nlogger.info("{} - finish prediction".format(name))

