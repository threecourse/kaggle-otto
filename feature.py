import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import StratifiedKFold
from util import Util, Config
from sklearn.preprocessing import StandardScaler
from data_loading import DataLoading

class Feature:

    @classmethod
    def load_feature(cls, i_fold, scaling=False):
        idx_tr, idx_te = DataLoading.load_index(i_fold)
        feature = DataLoading.load_feature_all()
        if scaling:
            # caution: to set values, check dtype
            feature = feature.astype(float)
            val = StandardScaler().fit_transform(feature)
            feature.values[:, :] = val
        return feature.iloc[idx_tr, :], feature.iloc[idx_te, :]

    @classmethod
    def load_feature_transform(cls, i_fold, method, scaling=False):
        # TODO: columns should be same
        idx_tr, idx_te = DataLoading.load_index(i_fold)
        feature = DataLoading.load_feature_all()
        feature = feature.astype(float)
        feature.values[:, :] = method(feature.values)

        if scaling:
            feature.values[:, :] = StandardScaler().fit_transform(feature)
        return feature.iloc[idx_tr, :], feature.iloc[idx_te, :]

    @classmethod
    def load_values(cls, i_fold, method, scaling=False):
        # return numpy values

        idx_tr, idx_te = DataLoading.load_index(i_fold)
        feature = DataLoading.load_feature_all()
        feature = feature.astype(float)
        values = method(feature.values)

        if scaling:
            values = StandardScaler().fit_transform(values)
        return values[idx_tr, :], values[idx_te, :]


    @classmethod
    def load_stacking_values(cls, name):

        n = len(DataLoading.load_id_all())
        print n
        values = np.zeros((n, Config.num_classes))

        folds = 5
        for i_fold in range(folds) + [-1]:
            _, idx = DataLoading.load_index(i_fold)
            preds = Util.load("../model/stack/{}_fold{}.pkl".format(name, i_fold))
            values[idx] = preds

        return values

class Transform:

    @classmethod
    def test_transform(cls, method):
        feature = DataLoading.load_feature_all()
        values = feature.values[:1000, :]
        Util.to_csv(pd.DataFrame(values), "../temp/values_base.txt")
        pd.DataFrame(values).to_csv()
        transformed = method(values)
        Util.to_csv(pd.DataFrame(transformed), "../temp/values_transformed.txt")

    @classmethod
    def nnb(cls, values):
        values = np.minimum(values, 10.0)
        return values

    @classmethod
    def nnc(cls, values):
        row_sum = values.sum(axis=1).reshape(-1, 1)
        row_sum = np.where(row_sum == 0.0, 1.0, row_sum)
        print row_sum.shape
        values /= row_sum
        return values

    @classmethod
    def nnd(cls, values):
        from sklearn.feature_extraction.text import TfidfTransformer
        tfidf = TfidfTransformer()
        values = tfidf.fit_transform(values).toarray()
        return values

    @classmethod
    def nne(cls, values):
        from sklearn.feature_extraction.text import TfidfTransformer
        tfidf = TfidfTransformer()
        values = tfidf.fit_transform(values).toarray()
        return values

    @classmethod
    def dummy(cls, values):
        return np.random.random_sample(size=values.shape)

    @classmethod
    def xgbz(cls, values):

        features = [values]
        features.append(np.floor(np.log2(values + 1)))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(3))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(4))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(5))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(6))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(7))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(8))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(9))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(12))))
        features.append(np.floor(np.divide(np.log(values + 1), np.log(13))))
        features.append(np.floor(np.log(values + 1)))
        features.append(np.floor(np.sqrt(values + 1)))
        features.append(np.power(values + 1, 2))

        ret = np.hstack(features)
        print ret.shape
        return ret
