import tks_util
from feature import Feature
import numpy as np

i_fold = 0
X = Feature.load_feature(i_fold, scaling=False)[0]
print X.shape
X = X.values

import sklearn.preprocessing as pp
po = .6

m = 130
R = tks_util.col_k_ones_matrix(X.shape[1], m, k_min=2, k_max=4, seed=71)
R.data = np.random.choice([1, -1], R.data.size)
X3 = X * R
X1 = np.sign(X3) * np.abs(X3) ** po
scaler = pp.StandardScaler()
X2 = scaler.fit_transform(X1)
