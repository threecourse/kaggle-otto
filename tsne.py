import sklearn.base
import bhtsne
import numpy as np

class BHTSNE(sklearn.base.BaseEstimator, sklearn.base.TransformerMixin):

    def __init__(self, dimensions=2, perplexity=30.0, theta=0.5, rand_seed=-1):
        self.dimensions = dimensions
        self.perplexity = perplexity
        self.theta = theta
        self.rand_seed = rand_seed

    def fit_transform(self, x):
        return bhtsne.tsne(
            x.astype(np.float64), dimensions=self.dimensions, perplexity=self.perplexity, theta=self.theta,
            rand_seed=self.rand_seed)

# http://iwiwi.hatenadiary.jp/entry/2016/09/24/230640
# https://github.com/dominiek/python-bhtsne
# 1000 -> 5sec, 10000-> 111sec

from data_loading import DataLoading
from feature import Feature

n = 1000
feat, _ = Feature.load_feature(-1)
target, _ = DataLoading.load_target(-1)
tsne = BHTSNE(dimensions=3, perplexity=50, theta=0.5, rand_seed=71).fit_transform(feat[:n])

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

def randrange(n, vmin, vmax):
    return (vmax - vmin)*np.random.rand(n) + vmin

fig = plt.figure()
# ax = fig.add_subplot(111)
ax = fig.add_subplot(111, projection='3d')
ax.scatter(tsne[:, 0], tsne[:, 1], tsne[:, 2], c=target.values[:n], cmap= plt.get_cmap("jet"))

plt.show()