import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import StratifiedKFold
from util import Util
from sklearn.preprocessing import StandardScaler

class DataLoading:

    @classmethod
    def load_id_all(cls):
        return Util.load("../model/idx/id.pkl")

    @classmethod
    def load_feature_all(cls):
        return Util.load("../model/idx/feature.pkl")

    @classmethod
    def load_target_all(cls):
        return Util.load("../model/idx/target.pkl")

    @classmethod
    def load_is_train_all(cls):
        return Util.load("../model/idx/is_train.pkl")

    @classmethod
    def load_id(cls, i_fold):
        idx_tr, idx_te = cls.load_index(i_fold)
        values = cls.load_id_all()
        return values[idx_tr], values[idx_te]

    @classmethod
    def load_target(cls, i_fold):
        idx_tr, idx_te = cls.load_index(i_fold)
        values = cls.load_target_all()
        return values[idx_tr], values[idx_te]

    @classmethod
    def load_index(cls, i_fold):
        idx_tr = Util.load("../model/idx/train_{}.pkl".format(i_fold))
        idx_te = Util.load("../model/idx/test_{}.pkl".format(i_fold))
        return idx_tr, idx_te

