from runner import Runner
from model_xgb import ModelSetXgb
import model_keras
import numpy as np

keras_params = {}
keras_params['lr'] = 0.02
keras_params['momentum'] = 0.9
keras_params['decay'] = 5e-5

keras_params['h1'] = 2048 # 300 # 2048
keras_params['h2'] = 4896 # 300 # 4896
keras_params['h3'] = 2048 # 100 # 2048
keras_params['dropout_input'] = 0.1
keras_params['dropout1'] = 0.6
keras_params['dropout2'] = 0.6
keras_params['dropout3'] = 0.6
keras_params['nb_epochs'] = 260
keras_params['batch_size'] = 64
keras_params['hidden_layers'] = 3
keras_params['init'] = "he_uniform"

from model import Model, ModelSet, ModelSetBagging
from model_keras import ModelKeras
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import Adadelta, SGD
from keras.layers.normalization import BatchNormalization
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint
from keras.utils.np_utils import to_categorical
from feature import Feature
from feature import Transform
from data_loading import DataLoading


class ModelKeras3(ModelKeras):

    def construct(self, input_dim):

        seed = self.prms["seed"] if self.prms.has_key("seed") else 71
        i_bag = self.prms["i_bag"] if self.prms.has_key("i_bag") else 0
        np.random.seed(seed + i_bag)

        if not self.prms.has_key("init"):
            init = 'glorot_normal'
        else:
            init = self.prms["init"]

        activation = 'relu'
        num_classes = 9

        optimizer = SGD(lr=self.prms["lr"], momentum=self.prms["momentum"], decay=self.prms["decay"], nesterov=True, clipnorm=1.15)

        hidden_layers = self.prms["hidden_layers"]
        assert (hidden_layers in [2, 3])

        model = Sequential()
        model.add(Dropout(self.prms["dropout_input"], input_shape=(input_dim, )))
        model.add(Dense(self.prms["h1"], init=init))
        model.add(Activation(activation))
        model.add(Dropout(self.prms["dropout1"]))
        model.add(Dense(self.prms["h2"], init=init))
        model.add(Activation(activation))
        model.add(Dropout(self.prms["dropout2"]))
        if hidden_layers == 3:
            model.add(Dense(self.prms["h3"], init=init))
            model.add(Activation(activation))
            model.add(Dropout(self.prms["dropout3"]))
        model.add(Dense(self.nb_classes, init=init))
        model.add(Activation('softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=optimizer)

        self.model = model


class ModelSetKeras3(ModelSet):

    def load_x(self, i_fold):
        return Feature.load_feature(i_fold, scaling=True)

    def get_model(self):
        return ModelKeras3()

Runner.run(ModelSetKeras3, "keras3", None, keras_params, train=True, test=True)
