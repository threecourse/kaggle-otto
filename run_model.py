from runner import Runner
import model_xgb

xgb_params = {}
xgb_params['objective'] = 'multi:softprob'
xgb_params['eta'] = 0.125
xgb_params['max_depth'] = 4
xgb_params['eval_metric'] = 'mlogloss'
xgb_params['silent'] = 1
# xgb_params['nthread'] = 6
xgb_params['num_class'] = 9
xgb_params['subsample'] = 0.8
xgb_params['colsample_bylevel'] = 0.5
xgb_params['colsample_bytree'] = 0.85
xgb_params['min_child_weight'] = 1
xgb_params['seed'] = 71
xgb_params["n_rounds"] = 500 # can go more

xgb_params_small = dict(xgb_params)
xgb_params_small["n_rounds"] = 5

xgb_params_ens = dict(xgb_params)
xgb_params_ens["n_rounds"] = 100

# Runner.run(model_xgb.ModelSetXgb, "xgb_basic", None, xgb_params, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_ens, "xgb_ens", ["keras_nnb", "xgb_basic"], xgb_params_ens, train=True, test=True)
# Runner.run(ModelSetXgb, "xgb_small", None, xgb_params_small, train=True, test=True)