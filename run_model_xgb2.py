from runner import Runner
import model_xgb

xgb_params = {}
xgb_params['objective'] = 'multi:softprob'
xgb_params['eta'] = 0.125
xgb_params['max_depth'] = 4
xgb_params['eval_metric'] = 'mlogloss'
xgb_params['silent'] = 1
# xgb_params['nthread'] = 6
xgb_params['num_class'] = 9
xgb_params['subsample'] = 0.8
xgb_params['colsample_bylevel'] = 0.5
xgb_params['colsample_bytree'] = 0.85
xgb_params['min_child_weight'] = 1
xgb_params['seed'] = 71
xgb_params["n_rounds"] = 500 # can go more

xgb_params_small = dict(xgb_params)
xgb_params_small["n_rounds"] = 5

xgb_params_xgbz0 = {}
xgb_params_xgbz0['objective'] = 'multi:softprob'
xgb_params_xgbz0['eta'] =  0.05
xgb_params_xgbz0['eval_metric'] = 'mlogloss'
xgb_params_xgbz0['silent'] = 1
# xgb_params['nthread'] = 6
xgb_params_xgbz0['num_class'] = 9
# xgb_params_xgbz0['subsample'] = 0.8
xgb_params_xgbz0['seed'] = 71
xgb_params_xgbz0['colsample_bylevel'] = 0.012
xgb_params_xgbz0['colsample_bytree'] = 1.0
xgb_params_xgbz0['min_child_weight'] = 5
xgb_params_xgbz0['max_depth'] = 50
xgb_params_xgbz0["n_rounds"] = 500

xgb_params_xgbz1 = dict(xgb_params_xgbz0)
xgb_params_xgbz1['seed'] = 72
xgb_params_xgbz1['colsample_bylevel'] = 0.024
xgb_params_xgbz1['colsample_bytree'] = 1.0
xgb_params_xgbz1['min_child_weight'] = 5
xgb_params_xgbz1['max_depth'] = 50
xgb_params_xgbz1["n_rounds"] = 400

xgb_params_xgbz2 = dict(xgb_params_xgbz0)
xgb_params_xgbz2['seed'] = 73
xgb_params_xgbz2['colsample_bylevel'] = 0.012
xgb_params_xgbz2['colsample_bytree'] = 1.0
xgb_params_xgbz2['min_child_weight'] = 5
xgb_params_xgbz2['max_depth'] = 30
xgb_params_xgbz2["n_rounds"] = 550

xgb_params_xgbz3 = dict(xgb_params_xgbz0)
xgb_params_xgbz3['seed'] = 74
xgb_params_xgbz3['colsample_bylevel'] = 0.012
xgb_params_xgbz3['colsample_bytree'] = 1.0
xgb_params_xgbz3['min_child_weight'] = 3
xgb_params_xgbz3['max_depth'] = 50
xgb_params_xgbz3["n_rounds"] = 400

xgb_params_xgbz4 = dict(xgb_params_xgbz0)
xgb_params_xgbz4['seed'] = 75
xgb_params_xgbz4['colsample_bylevel'] = 0.018
xgb_params_xgbz4['colsample_bytree'] = 1.0
xgb_params_xgbz4['min_child_weight'] = 5
xgb_params_xgbz4['max_depth'] = 50
xgb_params_xgbz4["n_rounds"] = 400

xgb_params_xgbz5 = dict(xgb_params_xgbz0)
xgb_params_xgbz5['seed'] = 76
xgb_params_xgbz5['colsample_bylevel'] = 0.012
xgb_params_xgbz5['colsample_bytree'] = 1.0
xgb_params_xgbz5['min_child_weight'] = 8
xgb_params_xgbz5['max_depth'] = 50
xgb_params_xgbz5["n_rounds"] = 650

xgb_params_xgbz6 = dict(xgb_params_xgbz0)
xgb_params_xgbz6['seed'] = 77
xgb_params_xgbz6['colsample_bylevel'] = 0.012
xgb_params_xgbz6['colsample_bytree'] = 1.0
xgb_params_xgbz6['min_child_weight'] = 5
xgb_params_xgbz6["max_depth"] = 40
xgb_params_xgbz6["n_rounds"] = 500

xgb_params_xgbz7 = dict(xgb_params_xgbz0)
xgb_params_xgbz7['seed'] = 78
xgb_params_xgbz7['colsample_bylevel'] = 0.024
xgb_params_xgbz7['colsample_bytree'] = 1.0
xgb_params_xgbz7['min_child_weight'] = 5
xgb_params_xgbz7["max_depth"] = 14
xgb_params_xgbz7["n_rounds"] = 750

xgb_params_xgbz8 = dict(xgb_params_xgbz0)
xgb_params_xgbz8['seed'] = 79
xgb_params_xgbz8['colsample_bylevel'] = 0.036
xgb_params_xgbz8['colsample_bytree'] = 1.0
xgb_params_xgbz8['min_child_weight'] = 5
xgb_params_xgbz8["max_depth"] = 14
xgb_params_xgbz8["n_rounds"] = 650

xgb_params_xgbz9 = dict(xgb_params_xgbz0)
xgb_params_xgbz9['seed'] = 80
xgb_params_xgbz9['colsample_bylevel'] = 0.012
xgb_params_xgbz9['colsample_bytree'] = 1.0
xgb_params_xgbz9['min_child_weight'] = 5
xgb_params_xgbz9["max_depth"] = 19
xgb_params_xgbz9["n_rounds"] = 750

# Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz_small", None, xgb_params_small, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz0", None, xgb_params_xgbz0, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz1", None, xgb_params_xgbz1, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz2", None, xgb_params_xgbz2, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz3", None, xgb_params_xgbz3, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz4", None, xgb_params_xgbz4, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz5", None, xgb_params_xgbz5, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz6", None, xgb_params_xgbz6, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz7", None, xgb_params_xgbz7, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz8", None, xgb_params_xgbz8, train=True, test=True)
Runner.run(model_xgb.ModelSetXgb_xgbz, "xgb_xgbz9", None, xgb_params_xgbz9, train=True, test=True)
